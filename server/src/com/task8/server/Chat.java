package com.task8.server;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class Chat {

    private static final Set<String> clients = new HashSet<>();
    private static final Set<PrintWriter> writers = new HashSet<>();

    public synchronized static boolean contains(String name){
        return clients.contains(name);
    }

    public synchronized static void add(String name){
        clients.add(name);
    }

    public synchronized static void addWriter(PrintWriter writer){
        writers.add(writer);
    }

    public static Set<PrintWriter> getWriters() {
        return writers;
    }

    public static Set<String> getClients() {
        return clients;
    }

    public synchronized static String printClients(){
        return clients.toString();
    }


}
