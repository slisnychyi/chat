package com.task8.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Set;
import java.util.concurrent.Callable;

public class Worker implements Runnable {

    private String name;
    private BufferedReader in;
    private PrintWriter out;
    private Socket socket;

    public Worker(Socket ss) {
        this.socket = ss;
    }

    @Override
    public void run() {
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);

            while (true) {
                out.println("register");

                name = in.readLine();
                System.out.println(name);
                if (name == null) {
                    return;
                }
                if (!Chat.contains(name)) {
                    Chat.add(name);
                    break;
                } else {
                    out.println("Already exists.");
                }
            }

            out.println("accepted:" + name);
            out.println("clients:" + Chat.printClients());
            Chat.addWriter(out);

            while (true) {
                String input = in.readLine();
                if (input == null) {
                    return;
                }

                for (PrintWriter writer : Chat.getWriters()) {
                    writer.println("message:" + name + ": " + input);
                }
            }


        } catch (IOException e) {
            System.out.println("Can't establish clientSocket");
            e.printStackTrace();
        } finally {
            if (name != null) {
                Chat.getClients().remove(name);
            }
            if (out != null) {
                Chat.getWriters().remove(out);
            }
            try {
                socket.close();
            } catch (IOException ignored) {

            }
        }


    }

}
