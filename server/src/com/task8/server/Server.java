package com.task8.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    private static final int DEFAULT_PORT = 8080;
    private static final Scanner SCANNER = new Scanner(System.in);
    private static final ExecutorService executor = Executors.newFixedThreadPool(100);


    public static void main(String[] args) throws IOException {
        //int port = getPort();
        ServerSocket ss = new ServerSocket(8080, 100);
        try {
            while (true) {
                Socket accept = ss.accept();
                executor.submit(new Worker(accept));
            }
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        finally {
            ss.close();
            executor.shutdownNow();
        }

    }

    private static int getPort() {
        System.out.println("Establish server port (by default:8080) : ");
        while (true) {
            try {
                String userPort = SCANNER.next();
                if ("".equals(userPort)) {
                    return DEFAULT_PORT;
                } else {
                    return Integer.valueOf(userPort);
                }
            } catch (Exception ex) {
                System.out.println("wrong type. should be int.");
            }
        }
    }


}
