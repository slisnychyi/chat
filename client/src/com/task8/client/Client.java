package com.task8.client;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    private PrintWriter out;

    JFrame frame = new JFrame("Chat");
    private JTextField textField = new JTextField(73);
    private JTextArea textArea = new JTextArea(40, 80);
    private JTextArea userArea = new JTextArea("Registered Users:\n",40, 20);

    public Client() {
        MigLayout mig = new MigLayout();
        userArea.setEditable(false);
        textField.setEditable(false);
        textArea.setEditable(false);
        frame.setLayout(mig);
        Container pane = frame.getContentPane();

        pane.add(new JScrollPane(textArea));
        pane.add(userArea, "wrap");
        pane.add(new Label("Input text"), "split 2");
        pane.add(textField);

        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        textField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                out.println(textField.getText());
                textField.setText("");
            }
        });

    }

    public static void main(String[] args) throws IOException {
        Client c = new Client();
        c.run();



    }

    private void run() throws IOException {
        InetAddress address = InetAddress.getByName("127.0.0.1");
        Socket socket = new Socket(address , 8080);
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);
        while (true) {
            String line = in.readLine();
            if (line.startsWith("register")) {
                String name = getName();
                out.println(name);
            } else if (line.startsWith("accepted")) {
                textField.setEditable(true);
            }
            else if(line.startsWith("clients")){
                String[] users = line.substring(line.indexOf(":") + 1).replace("[", "").replace("]", "").split(",");
                for(String user : users) {
                    userArea.append(user + "\n");
                }

            }
            else if (line.startsWith("message")) {
                textArea.append(line.substring(line.indexOf(":")+1) + "\n");
            }
        }
    }

    private String getName() {
        return JOptionPane.showInputDialog(
                frame,
                "Choose a screen name:",
                "Screen name selection",
                JOptionPane.PLAIN_MESSAGE);
    }
}
